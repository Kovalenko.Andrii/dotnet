﻿using System;
using System.Collections;

namespace CsFirstLab
{
    class ResearchTeam : Team , INameAndCopy
    {
        #region
        public string ResearchTopic { get; set; }

        //internal TimeFrame ResearchDuration { get => ResearchDuration1; set => ResearchDuration1 = value; }
        internal ArrayList ListPublication { get; set; }
        #endregion

        public bool this[TimeFrame frame] => frame == ResearchDuration;

        public ResearchTeam()
        {
            ResearchTopic = "ResearchTopic";
            Name = "Name";
            RegistrationNumber = 0;
            ResearchDuration = TimeFrame.Year;
        }
        //(from t in teams where t.ToUpper().StartsWith("Б") select t).Count();
        public Paper LastPublication {
            get {
                if (ListPublication == null) return null;
                Paper lastPublic = new Paper();

                    lastPublic = ListPublication[0] as Paper; //?
                    for (int i = 0; i < ListPublication.Count; i++)
                    {
                        if(lastPublic.DatePublication < (ListPublication[i] as Paper).DatePublication)
                        {
                            lastPublic = ListPublication[i] as Paper;
                        }
                    }
                return lastPublic; }
        }
        Team Team { get
            {
                Team team = new Team();

                return team;
            } }
        public ArrayList ProjectMember { get; set; }
        internal TimeFrame ResearchDuration { get; set; }

        public void AddPapers(Paper[] papers)
        {
            ListPublication = new ArrayList(papers);
        }

        public override string ToString()
        {
            string listPublic="";
            if (ListPublication != null)
            {
                for (int i = 0; i < ListPublication.Count; i++)
                {
                    listPublic += ListPublication[i].ToString() + '\n';
                }
            }
            
            return ($"{Name} regist number: {RegistrationNumber} do {ResearchTopic} during {ResearchDuration} : \n{ listPublic}");
        }
        public string ToShortString()
        {
            return ($"{Name} registr number: {RegistrationNumber} do {ResearchTopic} during {ResearchDuration}.");
        }

        public ResearchTeam(string researchTopic, string nameOrg, int registNumber, TimeFrame researchDuration)
        {
            ResearchTopic = researchTopic;
            Name = nameOrg;
            RegistrationNumber = registNumber;
            ResearchDuration = researchDuration;
        }
        public bool Equals(ResearchTeam researchTeam)
        {
            return !ReferenceEquals(null, researchTeam) && (ReferenceEquals(this, researchTeam)
                || researchTeam.Name == Name 
                && researchTeam.RegistrationNumber == RegistrationNumber && researchTeam.ResearchTopic == ResearchTopic 
                && researchTeam.ResearchDuration == ResearchDuration);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as ResearchTeam);
        }
        public override int GetHashCode()
        {
            return (Name != null && RegistrationNumber != 0 && ResearchTopic != null && ResearchDuration != 0
                ? Name.GetHashCode() + RegistrationNumber.GetHashCode() + ResearchTopic.GetHashCode() + ResearchDuration.GetHashCode() : 0);
        }

        public static bool operator ==(ResearchTeam left, ResearchTeam right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ResearchTeam left, ResearchTeam right)
        {
            return !Equals(left, right);
        }

        new public object DeepCopy()
        {
            ResearchTeam researchTeam = new ResearchTeam();
            return researchTeam;
        }


    }
}
