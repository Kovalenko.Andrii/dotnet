﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsFirstLab
{
    class Team : INameAndCopy
    {
        public string Name { get; set; }

        public int RegistrationNumber {
            set
            {
                try
                {
                    if (value < 0)
                    {
                        throw new Exception("value < 0 is not valid");
                    }
                    else
                    {
                        RegistrationNumber = value;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                   // return new Team("notValidNumberTeam");  ?
                }
            }
            get { return RegistrationNumber; }
        }

        public string NameOrg { get; set; }

        public virtual object DeepCopy()
        {
            Team team = new Team(Name = this.Name);
            return new Team
            {
                Name = this.Name,
                RegistrationNumber = this.RegistrationNumber
            };
        }
        #region

        public bool Equals(Team team)
        {
            return !ReferenceEquals(null, team) && (ReferenceEquals(this, team)
                || team.Name == Name && team.NameOrg == NameOrg && team.RegistrationNumber == RegistrationNumber);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Team);
        }

        public override int GetHashCode()
        {
            return (Name != null && NameOrg != null && RegistrationNumber != 0
                ? Name.GetHashCode() + NameOrg.GetHashCode() + RegistrationNumber.GetHashCode() : 0);
        }
        public static bool operator ==(Team left, Team right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Team left, Team right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return $"Info about team : {Name} / {NameOrg} / {RegistrationNumber}";
        }

        public Team()
        {
            NameOrg = "Name Org";
            Name = "Name";
            RegistrationNumber = 0;
        }
        public Team(string nameOrg , int registrNumber)
        {
            NameOrg = nameOrg;
            RegistrationNumber = registrNumber;
        }
        public Team(string nameOrg)
        {
            Name = nameOrg;
            RegistrationNumber = 0;
        }
        #endregion
    }
}
