﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsFirstLab
{
    class Person
    {
        private string _firstName;
        private string _secondName;
        private System.DateTime _bornDate;

        public string firstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }
        public string secondName
        {
            get
            {
                return _secondName;
            }
            set
            {
                _secondName = value;
            }
        }
        public System.DateTime bornDate
        {
            get
            {
                return _bornDate;
            }
            set
            {
                _bornDate = value;
            }
        }
        public int bornDateYear
        {
            get
            {
                return _bornDate.Year;
            }
            set
            {
                _bornDate = new DateTime(value, _bornDate.Month, _bornDate.Day);
            }
        }

        public Person(string fn, string sn, System.DateTime bd)
        {
            _firstName = fn;
            _secondName = sn;
            _bornDate = bd;
        }
        public Person()
        {
            _firstName = "default firts name";
            _secondName = "dedault second name";
            _bornDate = new DateTime(1111, 11, 11);
        }
        public virtual string ToShortString()
        {
            return ($"{firstName} {secondName}");
        }
        public override string ToString()
        {
            return ($"{firstName} {secondName} born to y:{bornDate.Year} m:{bornDate.Month} d:{bornDate.Day}");
        }
        public bool Equals(Person person)
        {
            return !ReferenceEquals(null, person) && (ReferenceEquals(this, person) 
                || person.firstName == firstName && person.secondName == secondName && person.bornDate == bornDate);
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as Person);
        }
        public override int GetHashCode()
        {
            return (firstName != null && secondName != null && bornDate != null 
                ? firstName.GetHashCode()+secondName.GetHashCode()+bornDate.GetHashCode() : 0);
        }
        public static bool operator == (Person left, Person right)
        {
            return Equals(left, right);
        }

        public static bool operator != (Person left, Person right)
        {
            return !Equals(left, right);
        }
    }
}
