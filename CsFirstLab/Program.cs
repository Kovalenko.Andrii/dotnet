﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsFirstLab
{
    
    class Program
    {   
        static void showPerson()
        {
            Person person = new Person();
            Console.WriteLine(person.ToString());
            Console.WriteLine(person.ToShortString());
            Console.WriteLine(person.bornDateYear);
            person.bornDateYear = 1996;
            Console.WriteLine(person.bornDateYear);

            Console.WriteLine(person.firstName);
            person.firstName = "andrii";
            Console.WriteLine(person.firstName);

            DateTime dateTime = new DateTime(1996, 11, 15);
            Person persons = new Person("Andrii", "Kovalenko", dateTime);
            Console.WriteLine(persons);
            Console.WriteLine(persons.ToShortString());
        }
        static void arrayTimeComparison()
        {
            Console.WriteLine("NRows : | ; | _ NCols");
            string input = Console.ReadLine();
            Console.WriteLine(input);
            string[] inputs = input.Split(':', ';', '_');
            int nRows = 0;
            int nCols = 0;
            int.TryParse(inputs[0], out nRows);
            int.TryParse(inputs[1], out nCols);
            int start;
            int finish;
            Person[] persons = new Person[nRows * nCols];
            DateTime dateTime = new DateTime(1996, 11, 15);
            start = Environment.TickCount;
            for (int i = 0; i < nRows * nCols; i++)
            {
                persons[i] = new Person("Andrii", "Kovalenko", dateTime);
            }
            finish = Environment.TickCount;
            Console.WriteLine("Time for 1spaces:" + (finish - start));

            Person[,] persons2 = new Person[nCols, nRows];
            start = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nCols; j++)
                {
                    persons2[i, j] = new Person("Andrii", "Kovalenko", dateTime);
                }
            }
            finish = Environment.TickCount;
            Console.WriteLine("Time for 2spaces:" + (finish - start));
            Person[][] person3 = new Person[nCols][];
            start = Environment.TickCount;
            for (int i = 0; i < person3.Length; i++)
            {
                person3[i] = new Person[nRows];
                for (int j = 0; j < nRows; j++)
                {
                    person3[i][j] = new Person("Andrii", "Kovalenko", dateTime);
                }
            }
            finish = Environment.TickCount;
            Console.WriteLine("Time for Nspaces:" + (finish - start));
        }
        static void comparePerson()
        {
            Person person = new Person("andrii", "kovalenko", new DateTime(1996, 11, 15));
            Person personF = new Person("andrii", "kovalenko", new DateTime(1996, 11, 15));
            Person personS = new Person("andriy", "kovalenko", new DateTime(1996, 11, 15));
            Person personT = new Person("andrii", "kovalenko", new DateTime(1996, 11, 11));

            Console.WriteLine(person.Equals(personF));
            Console.WriteLine(person.Equals(personS));
            Console.WriteLine(person.Equals(personT));

            Console.WriteLine(person == personF);
            Console.WriteLine(person == personS);
            Console.WriteLine(person == personT);

            Console.WriteLine(person != personF);
            Console.WriteLine(person != personS);
            Console.WriteLine(person != personT);

            Console.WriteLine(person.GetHashCode());
            Console.WriteLine(personF.GetHashCode());
            Console.WriteLine(personS.GetHashCode());
        }
        static void comparePaper()
        {
            Person person = new Person("andrii", "kovalenko", new DateTime(1996, 11, 15));
            Person personF = new Person("andriy", "kovalenko", new DateTime(1996, 11, 15));
            Paper paper = new Paper("autobio", person, new DateTime(2019, 03, 26));
            Paper paperF = new Paper("autobio", person, new DateTime(2019, 03, 26));
            Paper paperS = new Paper("autobio", personF, new DateTime(2019, 03, 26));
            Paper paperT = new Paper("autobio1", person, new DateTime(2019, 03, 26));
            Paper paperFo = new Paper("autobio", personF, new DateTime(2019, 04, 26));

            Console.WriteLine(paper.Equals(paperF));
            Console.WriteLine(paper.Equals(paperS));
            Console.WriteLine(paper.Equals(paperT));
            Console.WriteLine(paper.Equals(paperFo));

            Console.WriteLine(paper == paperF);
            Console.WriteLine(paper == paperS);
            Console.WriteLine(paper == paperT);
            Console.WriteLine(paper == paperFo);

            Console.WriteLine(paper != paperF);
            Console.WriteLine(paper != paperS);
            Console.WriteLine(paper != paperT);
            Console.WriteLine(paper != paperFo);

            Console.WriteLine(paper.GetHashCode());
            Console.WriteLine(paperF.GetHashCode());
            Console.WriteLine(paperS.GetHashCode());
        }


        static void Main(string[] args)
        {

            //Paper paper = new Paper();

            //Person person = new Person("andrii", "kovalenko", new DateTime(1996, 11, 15));
            //Paper[] paper1 = new Paper[2];
            //paper1[0] = new Paper("autobio", person, new DateTime(2019, 03, 26));
            //paper1[1] = new Paper("autobio", person, new DateTime(2020, 03, 26));
            //Paper[] paper2 = new Paper[2];
            //paper2[0] = new Paper("autobio1", person, new DateTime(2019, 03, 26));
            //paper2[1] = new Paper("MyBook", person, new DateTime(2018, 03, 26));

            //ResearchTeam researchTeam = new ResearchTeam();
            //Console.WriteLine("Info about default team: " + researchTeam.ToShortString());
            //Console.WriteLine("comparison with " + researchTeam.ResearchDuration + " time");
            //Console.WriteLine(TimeFrame.Year.ToString() + " " + researchTeam[TimeFrame.Year]);
            //Console.WriteLine(TimeFrame.TwoYears.ToString() + " " + researchTeam[TimeFrame.TwoYears]);
            //Console.WriteLine(TimeFrame.Long.ToString() + " " + researchTeam[TimeFrame.Long]);

            //researchTeam.ResearchTopic = "MyTopic";
            //researchTeam.RegistrationNumber = 12345;
            //researchTeam.ResearchDuration = TimeFrame.Long;
            //researchTeam.NameOrg = "My organization";
            //researchTeam.ListPublication = paper1;
            //researchTeam.AddPapers(paper2);
            //Console.WriteLine(researchTeam);
            //if (researchTeam.LastPublication == null)
            //{
            //    Console.WriteLine("null");
            //}
            //else Console.WriteLine("Last publication: " + researchTeam.LastPublication);

            //Program.arrayTimeComparison();
            //Program.showPerson();



            Console.ReadKey();
        }
    }
}
