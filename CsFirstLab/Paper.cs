﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsFirstLab
{
    class Paper
    {
        public string NamePublication { get; set; }
        public Person PublicationAuthor { get; set; }
        public DateTime DatePublication { get; set; }

        virtual public object DeepCopy()
        {
            Paper paper = new Paper();
            return paper;
        }
        public Paper(string namePublication, Person author, System.DateTime datePublication)
        {
            NamePublication = namePublication;
            PublicationAuthor = author;
            DatePublication = datePublication;
        }
        public Paper()
        {
            NamePublication = "default name";
            PublicationAuthor = new Person("firstName","secondName", new DateTime(1111,11,12));
            DatePublication = new DateTime(1111,11,11);
        }

        public override string ToString()
        {
            return ($"{NamePublication} written by to {PublicationAuthor.ToShortString()} in {DatePublication.Year}.{DatePublication.Month}.{DatePublication.Day}");
        }

        public bool Equals(Paper paper)
        {
            return !ReferenceEquals(null, paper) && (ReferenceEquals(this, paper)
                || paper.NamePublication == NamePublication && paper.PublicationAuthor == PublicationAuthor && paper.DatePublication == DatePublication);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Paper);
        }

        public override int GetHashCode()
        {
            return (NamePublication != null && PublicationAuthor != null && DatePublication != null
                ? NamePublication.GetHashCode() + PublicationAuthor.GetHashCode() + DatePublication.GetHashCode() : 0);
        }

        public static bool operator == (Paper left, Paper right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Paper left, Paper right)
        {
            return !Equals(left, right);
        }


    }
    enum TimeFrame{
        Year,
        TwoYears,
        Long
    }
}
